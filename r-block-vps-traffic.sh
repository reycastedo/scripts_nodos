#!/bin/bash -e

######################################
#
# Bloquea y desbloquea el tráfico en servidores VPS
#
#   r-block-vps-traffic ACCIÓN [PARÁMETROS ADICIONALES]
#
#   ban             ctid mainIP full|mail ticket    Bloquea el tráfico entrante y saliente de un servidor
#   unban           ctid                            Desbloquea el tráfico entrante y saliente de un servidor
#   allow-src       ctid IP [IP] [IP] ...           Permite el acceso a un servidor bloqueado a una IP
#   disallow-src    ctid IP [IP] [IP] ...           Deniega el acceso a un servidor bloqueado a una IP previamente permitida
#   check-one       ctid [--auto]                   Comprueba el estado de bloqueo de un servidor
#   check-all       [--auto] [--verbose]            Comprueba todos los servidores que alguna vez han sido bloqueados
#   list-one        ctid                            Detalle de un servidor bloqueado
#   list-all                                        Lista los servidores bloqueados
#   help                                            Muestra esta ayuda
#
#  r-block-vps-traffic está pensado para automatizar el bloqueo y desbloqueo de tráfico en servidores VPS.
#  Aunque el script solicita una IP para realizar el bloqueo de un servidor, en realidad bloquea el tráfico
#  a todas las IPs de la máquina.
#  El segundo objetivo es mantener los bloqueos que se están perdiendo por causas no determinadas y eliminarlos
#  en caso de que un servidor sea eliminado del nodo, evitando así levantar una nueva máquina con el tráfico
#  bloqueado.
#
#  Para alcanzar el objetivo, para cada servidor bloqueado crea un archivo de configuración en
#  /Raiola/blocked_servers/CTID donde CTID es el ctid asignado por openvz al servidor que tiene el bloqueo.
#
#  De forma adicional se genera un cron que, a todas las horas en punto, comprueba que los bloqueos permanecen
#  activos, de no estarlo los reactiva. Este mismo cron comprueba que el servidor sigue activo (da igual que esté
#  apagado) y si no lo está, elimina el bloqueo.
#
#
#  Ejemplos de uso:
#   - Bloquear todo el tráfico de un servidor
#     r-block-vps-traffic ban 4586 174.25.4.13 full 251687
#
#   - Bloquear el tráfico de correo de un servidor
#     r-block-vps-traffic ban 4586 174.25.4.13 mail 251687
#
#   - Permitir el acceso desde dos IPs a un servidor bloqueado
#     r-block-vps-traffic allow-src 4586 255.5.254.132 123.56.84.92
#
#   - Denegar el acceso desde dos IPs a un servidor bloqueado
#     r-block-vps-traffic disallow-src 4586 255.5.254.132 123.56.84.92
#
#   - Comprobar que un servidor sigue bloqueado y activo, solicitando toma de decisión
#     r-block-vps-traffic check 4586
#
#   - Comprobar que un servidor sigue bloqueado y activo con toma de decisiones automática
#     r-block-vps-traffic check 4586 --auto
#
#   - Comprobar que todos los servidores sigue bloqueados y activos, solicitando toma de decisiones
#     r-block-vps-traffic check-all [--verbose]
#
#   - Comprobar que todos los servidores sigue bloqueados y activos con toma de decisiones automática y mostrando info por pantalla
#     r-block-vps-traffic check-all --auto --verbose
#
#   - Comprobar que todos los servidores sigue bloqueados y activos con toma de decisiones automática y mostrando
#     información de lo sucedido con cada máquina
#     r-block-vps-traffic check-all --auto --verbose
#
#   - Listar detalle de bloqueo de un servidor
#     r-block-vps-traffic list-one 4586
#
#   - Listar todos los servidores bloqueados
#     r-block-vps-traffic list-all
#
# ShellCheck disable=2126,2059,SC2155
#
# Fecha creación: 2019-10-09
# Autor: fran.rc@raiolanetworks.es
#
######################################


######################################
#                                    #
#        VARIABLES GLOBALES          #
#                                    #
######################################

declare -r BLOCKEDSERVERS="/Raiola/blocked_servers"
declare -r LOGFILE="/var/log/r-block-vps-traffic.log"
declare -r SCRIPTPATH=$( dirname "$0" )
declare -r SCRIPTNAME=$( basename "$0" )
declare -r SAVE_IPTABLES="/Raiola/iptables-save"
declare -ra REQS=( vzlist iptables )


######################################
#                                    #
#       FUNCIONES AUXILIARES         #
#                                    #
######################################

# Comprueba que el número de parámetros es correcto
# El código de salida es 0 si todo está correcto o 1 en caso contrario
#   Recibe:
#    |- arg1: acción a realizar
#    |- arg*: número indeterminado de strings
#   Devuelve: nada

function check_args() {
    declare -u action=$1

    [[ $# -ge 1 ]] && shift

    case "${action}" in

        CHECK-ONE )
            [[ $# -ge 1 && $# -le 2 ]] || display_message "e" "Número de parámetros incorrecto"
            check_ctid "$1"
            ;;


        BAN )
            [[ $# -eq 4 ]] || display_message "e" "Número de parámetros incorrecto"
            check_ctid "$1"

            declare ticket=$4
            [[ "${ticket}" =~ ^[0-9]+$ ]] || display_message "e" "El identificador de ticket deben ser un solo números: ${ticket}"
            ;;


        CHECK-ALL )
            [[ $# -ge 0 && $# -le 2 ]] || display_message "e" "Número de parámetros incorrecto"
            ;;


        LIST-ONE | UNBAN )
            [[ $# -eq 1 ]] || display_message "e" "Número de parámetros incorrecto"
            check_ctid "$1"
            ;;


        ALLOW-SRC | DISALLOW-SRC )
            [[ $# -ge 2 ]] || display_message "e" "Número de parámetros incorrecto"
            check_ctid "$1"
            ;;


        LIST-ALL )
            [[ $# -eq 0 ]] || display_message "e" "Número de parámetros incorrecto"
            ;;

        * )
            display_help
            ;;

    esac

}


# Comprueba que existe todo lo necesario para usar el script
#   Recibe:   nada
#   Devuelve: nada

function initialize() {
    declare req
    declare -i control

    control=0

    for req in "${REQS[@]}"
    do
        if ! command -v "${req}" &> /dev/null
        then
            display_message "i" "No se encuentra ${req}"
            control=1
        fi
    done

    [[ "${control}" -eq 1 ]] && exit 1

    ! [[ -d "${BLOCKEDSERVERS}" ]] && mkdir -p "${BLOCKEDSERVERS}"
    ! [[ -f "${LOGFILE}" ]] && touch "${LOGFILE}"

    if ! [[ -f /etc/cron.d/r-block-vps-traffic ]]
    then
        (
        echo "SHELL=/bin/bash"
        echo "PATH=/sbin:/bin:/usr/sbin:/usr/bin"
        echo "MAILTO=raiolanetworks.n2@gmail.com"
        echo "HOME=/root"
        echo
        echo "0 * * * * root /Raiola/scripts/r-block-vps-traffic.sh check-all --auto"
        ) > /etc/cron.d/r-block-vps-traffic

        chown root:root /etc/cron.d/r-block-vps-traffic
        chmod 644 /etc/cron.d/r-block-vps-traffic
    fi

    if ! [[ -f /etc/logrotate.d/r-block-vps-traffic ]]
    then
        (
        echo "/var/log/r-block-vps-traffic.log {"
        echo "weekly"
        echo "rotate 8"
        echo "missingok"
        echo "notifempty"
        echo "delaycompress"
        echo "compress"
        echo "create 0640 root root"
        echo "}"
        ) > /etc/logrotate.d/r-block-vps-traffic

        chown root:root /etc/logrotate.d/r-block-vps-traffic
        chmod 644  /etc/logrotate.d/r-block-vps-traffic
    fi
}


# Muestra la ayuda del script
#   Recibe:   nada
#   Devuelve: nada

function display_help() {
    head -n +17 "${SCRIPTPATH}/${SCRIPTNAME}" | tr -d "#" | tail -n +4 | sed -e 's/^[ \t]*//'
    exit 0
}


# Muestra un mensaje de error por pantalla
#   Recibe:
#    |- arg1: tipo de error
#    |- arg2: mensaje a mostrar
#   Devuelve: nada

function display_message() {
    declare -u errorType=$1
    shift

    case "${errorType}" in

        E )
            echo -e " [e] $*" >&2
            exit 1
            ;;

        W )
            echo -e " [w] $*"
            exit 2
            ;;

        I )
            echo -e " [i] $*"
            ;;
    esac
}


# Escribe el texto recibido en el archivo de log
#   Recibe:   string de tamaño indeterminado
#   Devuelve: nada

function log() {
    declare actualDate

    actualDate=$( date "+%d-%m-%Y %H:%M:%S" )

    echo "${actualDate} - $*" >> "${LOGFILE}"
}


# Guarda las reglas de IPTables actuales
#   Recibe: nada
#   Devuelve: nada

function save_iptables() {
    declare actualDate

    ! [[ -d "${SAVE_IPTABLES}" ]] && mkdir -p "${SAVE_IPTABLES}"

    actualDate=$( date "+%d-%m-%Y %H:%M:%S" )

    iptables-save > "${SAVE_IPTABLES}/iptables-${actualDate}"

    find "${SAVE_IPTABLES}" -mtime +30 -delete
}


# Comprueba si un CTID de solusvm existe.
# El código de salida es 0 si si xiste y 1 en caso contrario.
#   Recibe:
#    |- arg1: ctid del vps
#   Devuelve: nada

function ctid_exists() {
    declare -i CTIDexists=1
    declare -i ctid=$1

    if vzlist "${ctid}" &> /dev/null
    then
        CTIDexists=0
    fi

    return "${CTIDexists}"
}


# Comprueba si un CTID y una IP pertenecen al mismo servidor.
# El código de salida es 0 en caso afirmativo y 1 en caso contrario.
#   Recibe:
#    |- arg1: ctid del vps
#    |- arg2: IP principal del servidor
#   Devuelve: nada

function ctid_matches_ip() {
    declare -i isActive=1
    declare -i ctid=$1
    declare mainIP=$2

    if vzlist -Ho ip "${ctid}" | grep -qws "${mainIP}"
    then
        isActive=0
    fi

    return "${isActive}"
}


# Comprueba si el servidor ya se encuentra bloqueado previamente.
# El código de salida de la función es 0 si no está bloqueado y 1 si lo está
#   Recibe:   ctid
#   Devuelve: nada

function already_banned() {
    declare -i alreadyBanned=1
    declare -i ctid=$1

    if [[ -f "${BLOCKEDSERVERS}/${ctid}" ]]
    then
        alreadyBanned=0
    fi

    return "${alreadyBanned}"
}


# Comprueba si el ctid recibido tiene un formato válido.
# Si no tiene un formato válido muestra un mensaje de error.
#   Recibe: ctid
#   Devuelve: nada

function check_ctid() {
    declare ctid=$1

    if ! [[ -n "${ctid}" && "${ctid}" =~ ^[0-9]+$ ]]
    then
        display_message "e" "El ctid deben ser solo números: ${ctid}"
    fi
}


# Comprueba si un argumento recibido es una IP válida
# El código de salida es 0 si el argumento recibido es una IP y 1 en caso contrario
#   Recibe:   string
#   Devuelve: nada

function is_valid_ip() {
    declare digit
    declare -i isValidIP=0
    declare -a IPdigits
    declare ip=$1

    mapfile -t IPdigits < <( tr "." "\n" <<< "${ip}" )

    if ! [[ "${ip}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ && "${#IPdigits[@]}" -eq 4 ]]
    then
        isValidIP=1
    fi

    for digit in "${IPdigits[@]}"
    do
        [[ "${digit}" -ge 0 && "${digit}" -le 255 ]] || isValidIP=1
    done

    return "${isValidIP}"
}


# Obtiene todas las IPs pertenecientes a un servidor menos la principal y las imprime.
#   Recibe:   ctid
#   Devuelve: nada

function get_additional_ips() {
    declare -i ctid=$1
    declare -a vpsIPs

    mapfile -t vpsIPs < <( vzlist -Ho ip "${ctid}"| tr " " "\n" )

    echo "${vpsIPs[@]}"
}


# Comprueba si el número de IPs en el archivo de conf y el número de IPs del VPS coinciden
#   Recibe:   ctid
#   Devuelve: nada

function has_same_ips() {
    declare -i ctid=$1
    declare -i ticket
    declare -a vpsIPs allowedIPs actualVpsIPs differentIPS addedIPs deletedIPs
    declare blockType

    source "${BLOCKEDSERVERS}/${ctid}"

    mapfile -t actualVpsIPs < <( get_additional_ips "${ctid}" | tr " " "\n" )
    mapfile -t differentIPS < <( echo "${actualVpsIPs[@]}" "${vpsIPs[@]}" |  tr " " "\n" | sort | uniq -c | grep -v " 2 " | awk '{print $2}' )

    [[ "${#differentIPS[@]}" -eq 0 ]] && return 0

    for ip in "${differentIPS[@]}"
    do
        if echo "${vpsIPs[*]}" | tr " " "\n" | grep -Eq "${ip}"
        then
            deletedIPs+=${ip}
        else
            addedIPs+=${ip}
        fi
    done

    display_message "i" "CTID ${ctid}: el número de IPs en el VPS ha variado desde el bloqueo"
    log "CTID ${ctid} - el número de IPs en el VPS ha variado desde el bloqueo"

    if [[ "${#addedIPs[@]}" -gt 0 ]]
    then
        display_message "i" "Añadidas : ${addedIPs[*]}"
        log "CTID ${ctid} - IPs añadidas - ${addedIPs[*]}"
    fi

    if [[ "${#deletedIPs[@]}" -gt 0 ]]
    then
        display_message "i" "Eliminadas: ${deletedIPs[*]}"
        log "CTID ${ctid} - IPs eliminadas - ${deletedIPs[*]}"
    fi

    return 1
}


# Comprueba si una IP tiene el acceso permitido a un servidor
# Su código de salida es 0 si está permitida y 1 en caso contrario
#   Recibe:
#    |- arg1: ctid del vps
#    |- arg2: IP a comprobar
#   Devuelve: nada

function is_allowed_ip() {
    declare -i ctid ticket
    declare testIP allowedIP blockType
    declare -a allowedIPs vpsIPs

    ctid=$1
    testIP=$2

    source "${BLOCKEDSERVERS}/${ctid}"

    for allowedIP in "${allowedIPs[@]}"
    do
        if [[ "${testIP}" == "${allowedIP}" ]]
        then
            return 0
        fi
    done

    return 1
}


# Guarda el registro de bloqueo de un servidor
#   Recibe:
#    |- arg1: ctid
#    |- arg2: tipo de bloqueo
#    |- arg3: ticket en que se informa al cliente
#    |- arg*: IPs del VPS
#   Devuelve: nada

function save_ban() {
    declare -i ctid ticket
    declare -a vpsIPs allowedIPs

    ctid=$1
    blockType=$2
    ticket=$3
    shift 3
    vpsIPs=( $@ )

    {
    echo "ctid=${ctid}"
    echo "blockType=${blockType}"
    echo "vpsIPs=( ${vpsIPs[*]} )"
    echo "allowedIPs=( ${allowedIPs[*]} )" # Siempre estará vacío, se guarda para ser usado a futro si se necesita
    echo "ticket=${ticket}"
    } > "${BLOCKEDSERVERS}/${ctid}"
}


# Guarda en el fichero de registro de bloqueo las IPs que se han permitido
#   Recibe:
#   |- arg1: ctid
#   |- arg*: número indeterminado de IPs
#   Devuelve: nada

function save_allow_src() {
    declare -i ctid ticket
    declare -a newAllowedIPs allowedIPs vpsIPs
    declare blockType

    ctid=$1
    shift
    newAllowedIPs=( "${@}" )

    source "${BLOCKEDSERVERS}/${ctid}"

    newAllowedIPs=( "${newAllowedIPs[@]}" "${allowedIPs[@]}" )

    if [[ "${#allowedIPs[@]}" -eq 0 ]]
    then
        sed -i "s|allowedIPs=(  )|allowedIPs=( ${newAllowedIPs[*]} )|g" "${BLOCKEDSERVERS}/${ctid}"
    else
        sed -i "s/allowedIPs=( ${allowedIPs[*]} )/allowedIPs=( ${newAllowedIPs[*]} )/g" "${BLOCKEDSERVERS}/${ctid}"
    fi
}


# Guarda en el fichero de registro de bloqueo las IPs que se han permitido
#   Recibe:
#   |- arg1: ctid
#   |- arg*: número indeterminado de IPs
#   Devuelve: nada

function save_disallow_src() {
    declare -i ctid ticket
    declare -a newAllowedIPs allowedIPs vpsIPs disallowedIPs newAllowedIPs
    declare blockType ip

    ctid=$1
    shift
    disallowedIPs=( "${@}" )

    source "${BLOCKEDSERVERS}/${ctid}"

    for ip in "${disallowedIPs[@]}"
    do
        newAllowedIPs=( "${allowedIPs[@]}" "${disallowedIPs[@]}" )
        mapfile -t newAllowedIPs < <( echo "${newAllowedIPs[@]}" | tr " " "\n" | sort | uniq -c | grep -v " 2 " | awk '{print $2}' )
    done

    if [[ "${#newAllowedIPs[@]}" -gt 0 ]]
    then
        sed -i "s/allowedIPs=( ${allowedIPs[*]} )/allowedIPs=( ${newAllowedIPs[*]} )/g" "${BLOCKEDSERVERS}/${ctid}"
    else
        sed -i "s/allowedIPs=( ${allowedIPs[*]} )/allowedIPs=(  )/g" "${BLOCKEDSERVERS}/${ctid}"
    fi
}


######################################
#                                    #
#       FUNCIONES PRINCIPALES        #
#                                    #
######################################

# Bloquea todo el tráfico de un VPS
#   Recibe:
#    |- arg1: ctid
#    |- arg2: ticket
#    |- arg*: número indeterminado de IPs
#   Devuelve: nada

function ban_all_traffic() {
    declare IP
    declare -i ctid=$1
    declare -i ticket=$2
    declare verbose=$3
    shift 3
    declare -a vpsIPs=( $@ )

    for IP in "${vpsIPs[@]}"
    do
        iptables -I FORWARD -d "${IP}" -p tcp -m comment --comment "CTID ${ctid}" -j DROP
        iptables -I FORWARD -s "${IP}" -p tcp -m comment --comment "CTID ${ctid}" -j DROP
        iptables -I FORWARD -d "${IP}" -p udp -m comment --comment "CTID ${ctid}" -j DROP
        iptables -I FORWARD -s "${IP}" -p udp -m comment --comment "CTID ${ctid}" -j DROP
    done

    [[ "${verbose}" == "--verbose" ]] && display_message "i" "CTID ${ctid}: bloqueo de tipo FULL en base a ticket ${ticket}"
    log "CTID ${ctid} - full ban - T#${ticket}"
}


# Bloquea el tráfico saliente al puerto 25 de un VPS
#   Recibe:
#    |- arg1: ctid
#    |- arg2: ticket
#    |- arg*: número indeterminado de IPs
#   Devuelve: nada

function ban_mail_traffic() {
    declare IP
    declare -i ctid=$1
    declare -i ticket=$2
    declare verbose=$3
    shift 3
    declare -a vpsIPs=( $@ )

    for IP in "${vpsIPs[@]}"
    do
        iptables -I FORWARD -p tcp -s "${IP}" --dport 25 -m comment --comment "CTID ${ctid}" -j DROP
    done

    [[ "${verbose}" == "--verbose" ]] && display_message "i" "CTID ${ctid}: bloqueo de tipo MAIL en base a ticket ${ticket}"
    log "CTID ${ctid} - mail ban - T#${ticket}"
}


# Desbloquea el tráfico de un servidor
#   Recibe:
#    |- arg1: ctid
#    |- arg2(opcional): --verbose 
#   Devuelve: nada

function unban() {
    declare -a vpsIPs allowedIPs delIptablesRules
    declare blockType
    declare -i ticket rule
    declare -i ctid=$1
    declare -i verbose=0

    [[ "$2" == "--verbose" ]] && verbose=1

    source "${BLOCKEDSERVERS}/${ctid}"

    mapfile -t delIptablesRules < <( iptables -L FORWARD --line-numbers | grep -F "/* CTID ${ctid} */" | awk '{print $1}' | sort -nr )

    for rule in "${delIptablesRules[@]}"
    do
        iptables -D FORWARD "${rule}"
    done

    if [[ "${verbose}" -eq 1 ]] 
    then
        display_message "i" "CTID ${ctid}: desbloqueo de tipo ${blockType} en base a ticket ${ticket}"
    fi

    log "CTID ${ctid} - ${blockType} unban - T#${ticket}"

    rm -f "${BLOCKEDSERVERS}/${ctid}"
}


# Permite añadir exclusiones para que ciertas IPs tengan acceso al servidor
#   Recibe:
#    |- arg1: ctid
#    |- arg2: save (--save|--no-save) guarda o no la configuración realizada
#    |- arg3: verbose (--verbose|--no-verbose) muestra o no info por pantalla
#    |- arg*: número indeterminado de IPs
#   Devuelve: nada

function allow_src() {
    declare -i ctid=$1
    declare save=$2
    declare verbose=$3
    declare customerIP serverIP blockType ticket
    declare -a exceptionIPs vpsIPs allowedIPs

    shift 3

    exceptionIPs=( "$@" )

    source "${BLOCKEDSERVERS}/${ctid}"

    for customerIP in "${exceptionIPs[@]}"
    do
        for serverIP in "${vpsIPs[@]}"
        do
            iptables -I FORWARD -p tcp -s "$customerIP" -d "$serverIP" -m comment --comment "CTID ${ctid}" -j ACCEPT
            iptables -I FORWARD -p tcp -s "$serverIP" -d "$customerIP" -m comment --comment "CTID ${ctid}" -j ACCEPT
        done
    done

    if [[ "${verbose}" == "--verbose" ]]
    then
        for customerIP in "${exceptionIPs[@]}"
        do
            display_message "i" "CTID ${ctid}: se permite el tráfico desde IP ${customerIP}"
        done
    fi

    if [[ "${save}" == "--save" ]]
    then
        save_allow_src "${ctid}" "${exceptionIPs[@]}"
    fi

    log "CTID ${ctid} - excepción para ${exceptionIPs[*]} - T#${ticket}"
}


# Permite añadir exclusiones para que ciertas IPs tengan acceso al servidor
#   Recibe:
#    |- arg1: ctid
#    |- arg2: save (--save|--no-save) guarda o no la configuración realizada
#    |- arg3: verbose (--verbose|--no-verbose) muestra o no info por pantalla
#    |- arg*: número indeterminado de IPs
#   Devuelve: nada

function disallow_src() {
    declare -i ctid=$1
    declare save=$2
    declare verbose=$3
    declare customerIP serverIP blockType disallowIP
    declare -a disallowIPs vpsIPs allowedIPs delIptablesRules
    declare -i actuallyBanned ticket rule

    shift 3

    disallowIPs=( "$@" )

    source "${BLOCKEDSERVERS}/${ctid}"

    for disallowIP in "${disallowIPs[@]}"
    do
        mapfile -t delIptablesRules < <( iptables -L FORWARD --line-numbers | grep -F "/* CTID ${ctid} */" | grep -F "${disallowIP}" | awk '{print $1}' | sort -nr )

        for rule in "${delIptablesRules[@]}"
        do
            iptables -D FORWARD "${rule}"
        done
    done

    if [[ "${verbose}" == "--verbose" ]]
    then
        display_message "i" "CTID ${ctid}: se deniega el tráfico desde IP ${disallowIPs[*]}"
    fi

    save_disallow_src "${ctid}" "${disallowIPs[@]}"

    log "CTID ${ctid} - excepción deshabilitada para ${disallowIPs[*]} - T#${ticket}"
}


# Genera una lista con los servidores actualmente bloqueados y la imprime por pantalla
#   Recibe: nada
#   Devuelve: nada

function list_one_blocked() {
    declare blockType ticket format
    declare -a vpsIPs
    declare -i ctid=$1

    source "${BLOCKEDSERVERS}/${ctid}"

    format="   %-16s %-s\n"

    printf "\n"
    printf "$format" "CTID:" "${ctid}"
    printf "$format" "Bloqueo:" "${blockType}"
    printf "$format" "Ticket:" "${ticket}"
    printf "$format" "IPs bloqueadas:" "${vpsIPs[*]}"
    printf "$format" "IPs permitidas:" "${allowedIPs[*]}"
    printf "\n"
}


# Genera una lista con los servidores actualmente bloqueados y la imprime por pantalla
#   Recibe: nada
#   Devuelve: nada

function list_all_blocked() {
    declare header divider format vps blockType
    declare -i width ctid ticket
    declare -a vpsIPs allowedIPs

    for vps in "${BLOCKEDSERVERS}"/*
    do
        # Comprobamos que el directorio no esté vacío
        if [[ "${BLOCKEDSERVERS}/*" == "${vps}" ]]
        then
            display_message "w" "No hay servidores bloqueados"
        fi
    done

    divider===============================
    divider=${divider}${divider}${divider}
    header=" %-6s %-6s %-8s %-12s %-16s\n"
    format=" %-6s %-7s %-11s %-13s %-15s\n"
    width=50

    echo
    printf "$header" "TYPE" "CTID" "TICKET" "BANNED-IPS" "ALLOWED-IPS"
    printf "%$width.${width}s\n" "$divider"


    for vps in "${BLOCKEDSERVERS}"/*
    do
        source "${vps}"
        printf "${format}" "${blockType}" "${ctid}" "${ticket}" "${#vpsIPs[@]}" "${#allowedIPs[@]}"
    done

    echo
}


# Realiza un bucle sobre los archivos de configuración para comprobar que todos los VPS bloqueados siguen activos
#   Recibe:
#    |- arg* (opcionales): --auto --verbose
#   Devuelve: nada

function check_all_vps() {
    declare vps
    declare asumeYes=$1
    declare verbose=$2

    for vps in "${BLOCKEDSERVERS}"/*
    do
        vps=$( basename "${vps}" )
        if [[ "${vps}" == '*' ]]
        then
            if [[ "${verbose}" == "--verbose" ]]
            then
                display_message "w" "No hay servidores bloqueados"
            else
                exit 2
            fi
        fi

        check_one_vps "${vps}" "${asumeYes}" "${verbose}"
    done
}


# Comprueba si un servidor dado sigue activo y bloqueado
# Si el servidor ha sido eliminado y se llama la función con el argumento --auto, elimina las reglas de IPtables
# Si el servidor ha perdido el bloqueo y se llama a la función con --auto, reactiva el bloqueo
# Si se usa la opción --verbose en combinación con --auto, informará de las acciones realizadas
# Si se usa solamente --verbose, preguntará antes de realizar ninguna acción
#   Recibe:
#    |- arg1: ctid
#    |- arg* (opcionales): --auto --verbose
#   Devuelve: nada

function check_one_vps() {
    declare -i ticket numIPtables
    declare blockType input
    declare -a vpsIPs allowedIPs
    declare -i stillBlocked=1
    declare -i IPsChanged=0
    declare -i asumeYes=0
    declare -i verbose=0
    declare -i ctid=$1

    [[ $2 == "--auto" ]] && asumeYes=1
    [[ $3 == "--verbose" ]] && verbose=1

    declare -i isActive=0

    # Comprobamos si está bloqueado, si no lo está mostramos mensaje y salimos
    if already_banned "${ctid}"
    then
        source "${BLOCKEDSERVERS}/${ctid}"
    else
        display_message "w" "CTID ${ctid}: el servidor no está bloqueado"
        return 1
    fi

    # Comprobamos que el ctid sigue activo
    if ctid_exists "${ctid}"
    then
        isActive=1
    fi

    # Comprobamos si sigue bloqueado
    if [[ "${isActive}" -eq 1 ]]
    then
        numIPtables=$( iptables -L FORWARD --line-numbers | grep "CTID ${ctid}" | wc -l )

        [[ "${numIPtables}" -eq 0 ]] && stillBlocked=0
    fi

    # Comprobamos que el número de IPs en el VPS no ha cambiado
    if ! has_same_ips "${ctid}"
    then
        IPsChanged=1
    fi

    #################
    # LÓGICA SI EL SERVIDOR CONTINÚA ACTIVO
    #################


    # LAS IPS DEL SERVIDOR HAN CAMBIADO

    # Servidor activo, las IPs han cambiado y proceder de forma automática
    if [[ "${isActive}" -eq 1 && "${IPsChanged}" -eq 1 && "${asumeYes}" -eq 1 ]]
    then
        save_iptables
        restart_ban "${ctid}" "--no-verbose" "--force-new-ips"

        [[ "${verbose}" -eq 1 ]] && display_message "i" "CTID ${ctid}: bloqueo restablecido, las IPs del servidor habían cambiado"
        return 0
    fi

    # Servidor activo, las IPs han cambiado, no proceder de forma automática y modo verbose
    if [[ "${isActive}" -eq 1  && "${asumeYes}" -eq 0 && "${IPsChanged}" -eq 1 && "${verbose}" -eq 1 ]]
    then
        display_message "i" "CTID ${ctid}: las IPs del servidor han cambiado"
        read -p "¿Quieres resetear las reglas de IPtables?: [y/n] " input

        case "$input" in
            y | s | Y | S )
                save_iptables
                restart_ban "${ctid}" "--verbose" "--force-new-ips"
                display_message "i" "CTID ${ctid}: bloqueo restablecido, las IPs del servidor habían cambiado"
                return 0
                ;;
            * )
                return
                ;;
        esac
    fi


    # SE HAN PERDIDO LAS REGLAS DE IPTABLES

    # Servidor activo, no está bloqueado y proceder de forma automática
    if [[ "${isActive}" -eq 1  && "${asumeYes}" -eq 1 && "${stillBlocked}" -eq 0 ]]
    then
        save_iptables
        restart_ban "${ctid}" "--no-verbose"

        [[ "${verbose}" -eq 1 ]] && display_message "i" "CTID ${ctid}: bloqueo restablecido, se habían perdido reglas IPTABLES"
        return 0
    fi

    # Servidor activo, no bloqueado, no proceder de forma automática y modo verbose
    if [[ "${isActive}" -eq 1  && "${asumeYes}" -eq 0 && "${stillBlocked}" -eq 0 && "${verbose}" -eq 1 ]]
    then
        display_message "i" "CTID ${ctid}: el servidor no tiene el bloqueo activo"
        read -p "¿Quieres reactivar las reglas de IPtables?: [y/n] " input

        case "$input" in
            y | s | Y | S )
                save_iptables
                restart_ban "${ctid}" "--verbose"
                display_message "i" "CTID ${ctid}: bloqueo restablecido"
                return 0
                ;;
            * )
                return
                ;;
        esac
    fi

    #################
    # LÓGICA SI EL SERVIDOR YA NO SE ENCUENTRA ACTIVO
    #################

    # No activo y proceder de forma automática
    if [[ "${isActive}" -eq 0  && "${asumeYes}" -eq 1 ]]
    then
        save_iptables
        unban "${ctid}"
        [[ "${verbose}" -eq 1 ]] && display_message "i" "CTID ${ctid} no activo: el servidor ha sido desbloqueado"
        return 0
    fi

    # No activo, no proceder de forma automática y verbose
    if [[ "${isActive}" -eq 0  && "${asumeYes}" -eq 0 && "${verbose}" -eq 1 ]]
    then
        display_message "i" "CTID ${ctid} no está activo"
        read -p "¿Quieres eliminar las reglas de IPtables?: [y/n] " input

        case "$input" in
            y | s | Y | S )
                save_iptables
                unban "${ctid}"
                display_message "i" "CTID ${ctid} no activo: el servidor ha sido desbloqueado"
                return 0
                ;;
            * )
                return
                ;;
        esac
    fi

    # El servidor está bloqueado, no han ocurrido excepciones y se ha pedido información por pantalla
    if [[ "${verbose}" -eq 1 ]]
    then
        display_message "i" "CTID ${ctid}: mantiene el bloqueo y sigue activo"
    fi
}


# Establece un bloqueo sobre un servidor en base a un archivo de configuración previamente generado
#   Recibe:
#    |- arg1: ctid
#   Devuelve: nada

function restart_ban() {
    declare -i ticket
    declare blockType
    declare -a vpsIPs allowedIPs newVpsIPs
    declare -i ctid=$1
    declare verbose=$2
    declare forceNewIPs=$3

    source "${BLOCKEDSERVERS}/${ctid}"

    if [[ "${forceNewIPs}" == "--force-new-ips" ]]
    then
        mapfile -t newVpsIPs < <( get_additional_ips "${ctid}" | tr " " "\n" )
        sed -i "s/vpsIPs=( ${vpsIPs[*]} )/vpsIPs=( ${newVpsIPs[*]} )/g" "${BLOCKEDSERVERS}/${ctid}"
        unset vpsIPs
        vpsIPs=( "${newVpsIPs[@]}" )
    fi

    if [[ "${blockType}" == "full" ]]
    then
        ban_all_traffic "${ctid}" "${ticket}" "${verbose}" "${vpsIPs[@]}"

        # Restablecemos además las IP que se pueden conectar
        if [[ "${#allowedIPs[@]}" -ne 0 ]]
        then
            allow_src "${ctid}" "--no-save" "--no-verbose" "${allowedIPs[@]}"
        fi

    elif [[ "${blockType}" == "mail" ]]
    then
        ban_mail_traffic "${ctid}" "${ticket}" "${verbose}" "${vpsIPs[@]}"

    fi

    log "CTID ${ctid} - la configuración de bloqueo ha tenido que ser restaurada"
}


######################################
#                                    #
#         LÓGICA PRINCIPAL           #
#                                    #
######################################

function main() {

    initialize

    check_args "$@"

    declare -u action=$1
    shift

    case "${action}" in

        BAN )
            declare -i ctid=$1
            declare mainIP=$2
            declare blockType=$3
            declare -i ticket=$4
            declare input
            declare -i mustBan=0
            declare -a vpsIPs

            # Comprobamos que existe el ctid
            if ! ctid_exists "${ctid}"
            then
                display_message "e" "CTID ${ctid} incorrecto"
            fi

            # Comprobamos que el ctid y la IP concuerdan
            if ! ctid_matches_ip "${ctid}" "${mainIP}"
            then
                display_message "e" "El servidor no existe o el CTID: ${ctid} y la IP: ${mainIP} no concuerdan"
            fi

            # Comprobamos que no esté ya bloqueado
            if already_banned "${ctid}"
            then

                # Capturamos la excepción de que el servidor ya esté con un bloqueo de tipo mail y se le quiera poner full
                if [[ "${blockType}" == "full" ]] && grep -qws 'blockType=mail' "${BLOCKEDSERVERS}/${ctid}"
                then
                    read -p "El servidor tiene un bloqueo de correo, ¿quieres aplicar un bloqueo completo?: [y/n] " input

                    case "$input" in
                        y | s | Y | S )
                            save_iptables
                            unban "${ctid}" "--verbose"
                            mustBan=1
                            ;;
                        * )
                            :
                            ;;
                    esac
                fi

                if [[ "${mustBan}" -eq 0 ]]
                then
                    list_one_blocked "${ctid}"
                    display_message "e" "CTID ${ctid}: ya está bloqueado"
                fi
            fi

            mapfile -t vpsIPs < <( get_additional_ips "${ctid}" | tr " " "\n" )

            # Bloqueamos según el parámetro recibido
            if [[ "${blockType}" == "full" ]]
            then
                save_iptables
                ban_all_traffic "${ctid}" "${ticket}" "--verbose" "${vpsIPs[@]}"

            elif [[ "${blockType}" == "mail" ]]
            then
                save_iptables
                ban_mail_traffic "${ctid}" "${ticket}" "--verbose" "${vpsIPs[@]}"

            else
                display_message "w" "Solamente se bloquea con los argumentos mail y full"
            fi

            save_ban "${ctid}" "${blockType}" "${ticket}" "${vpsIPs[@]}"
            ;;


        UNBAN )
            declare -i ctid=$1

            if ! already_banned "${ctid}"
            then
                display_message "e" "CTID ${ctid}: el servidor no está bloqueado"
            fi

            save_iptables
            unban "${ctid}" "--verbose"
            ;;


        ALLOW-SRC )
            declare -i ctid=$1
            shift
            declare -a exceptionIPs=( $@ )
            declare customerIP

            # Comprobamos que está bloqueado
            if ! already_banned "${ctid}"
            then
                display_message "e" "CTID ${ctid}: el servidor no está bloqueado"
            fi

            # Comprobamos la validez de las IP
            for customerIP in "${exceptionIPs[@]}"
            do
                if ! is_valid_ip "${customerIP}"
                then
                    display_message "e" "La IP ${customerIP} no es válida"
                fi
            done

            source "${BLOCKEDSERVERS}/${ctid}"

            # Comprobamos que el ctid sigue activo
            if ! ctid_exists "${ctid}"
            then
                check_one_vps "${ctid}" "${asumeYes}" '--verbose'
                exit 1
            fi

            # Comprobamos que el bloqueo sea de tipo full
            if [[ "${blockType}" != "full" ]]
            then
                display_message "e" "CTID ${ctid}: no tiene un bloqueo completo"
            fi

            # Comprobamos que una IP no esté ya añadida como excepción
            for customerIP in "${exceptionIPs[@]}"
            do
                if is_allowed_ip "${ctid}" "${customerIP}"
                then
                    display_message "w" "CTID ${ctid}: IP ${customerIP} ya está permitida"
                fi
            done

            save_iptables
            allow_src "${ctid}" "--save" "--verbose" "${exceptionIPs[@]}"
            ;;


        DISALLOW-SRC )
            declare -i ctid=$1
            shift
            declare -a disallowIPs=( $@ )
            declare disallowIP

            # Comprobamos que está bloqueado
            if ! already_banned "${ctid}"
            then
                display_message "e" "CTID ${ctid}: el servidor no está bloqueado"
            fi

            # Comprobamos la validez de las IP
            for disallowIP in "${disallowIPs[@]}"
            do
                if ! is_valid_ip "${disallowIP}"
                then
                    display_message "e" "La IP ${disallowIP} no es válida"
                fi
            done

            source "${BLOCKEDSERVERS}/${ctid}"

            # Comprobamos que el ctid sigue activo
            if ! ctid_exists "${ctid}"
            then
                check_one_vps "${ctid}" "${asumeYes}" '--verbose'
                exit 1
            fi

            # Comprobamos que el bloqueo sea de tipo full
            if [[ "${blockType}" != "full" ]]
            then
                display_message "e" "CTID ${ctid}: no tiene un bloqueo completo"
            fi

            # Comprobamos que una IP no esté ya añadida como excepción
            for disallowIP in "${disallowIPs[@]}"
            do
                if is_allowed_ip "${ctid}" "${customerIP}"
                then
                    display_message "w" "CTID ${ctid}: IP ${customerIP} no está permitida"
                fi
            done

            save_iptables
            disallow_src "${ctid}" "--save" "--verbose" "${disallowIPs[@]}"
            ;;


        CHECK-ONE | CHECK-ALL )
            while (( ${#} ))
            do
                declare arg=$1

                if [[ "${action}" == "CHECK-ONE" && -z "${ctid}" ]]
                then
                    declare -i ctid="${arg}"
                    shift
                    continue
                fi

                case "${arg}" in
                    --auto )
                        declare asumeYes="--auto"
                        ;;
                    --verbose )
                        declare verbose="--verbose"
                        ;;
                    * )
                        display_message "e" "Argumento inválido: ${arg}"
                esac

            shift

            done

            if [[ "${action}" == "CHECK-ONE" ]]
            then
                if ! already_banned "${ctid}"
                then
                    display_message "e" "CTID ${ctid}: el servidor no está bloqueado"
                fi

                check_one_vps "${ctid}" "${asumeYes}" "--verbose"

            elif [[ "${action}" == "CHECK-ALL" ]]
            then
                # Incluímos esta opción para evitar que check-all se lance sin argumentos no haga nada
                [[ -z "${asumeYes}" && -z "${verbose}" ]] && declare verbose="--verbose"

                check_all_vps "${asumeYes}" "${verbose}"
            fi
            ;;


        LIST-ALL )
            list_all_blocked
            ;;


        LIST-ONE )
            declare -i ctid=$1

            if ! ctid_exists "${ctid}"
            then
                display_message "e" "CTID ${ctid} incorrecto"
            fi

            if ! already_banned "${ctid}"
            then
                display_message "e" "CTID ${ctid}: el servidor no está bloqueado"
            fi

            list_one_blocked "${ctid}"
            ;;

    esac

    exit 0
}

main "${@}"
